//[SECTION] Dependencies and Modules
  const mongoose =require("mongoose");

//[SECTION] Schema/Document Blueprint
  const orderSchema = new mongoose.Schema({
    totalAmount:{
      type: Number,
      //required: [true, 'Total Amount is Required']
    },
    purchasedOn:{
      type: Date,
      default: new Date()
    },
    userId:{
      type: String,
      required: [true, "User's ID is Required"]
    },
    orders:[{
        productId:{
          type: String,
          //required: [true,"Product ID is Required"]
        },
        quantity:{
          type: Number,
          //required: [true, "Quantity is Required"]
        }
      }
    ]
  });

//[SECTION] Model
  const Order = mongoose.model("Order", orderSchema);
  module.exports = Order;

