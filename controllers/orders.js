//[SECTION] Dependencies and Modules
	const auth = require("../auth")
	const User = require('../models/User');
	const Product = require('../models/Product');
	const Order = require('../models/Order');
	const bcrypt = require("bcrypt");
	const dotenv = require("dotenv");

//[SECTION] Environment Setup
	dotenv.config();
	const salt = parseInt(process.env.SALT);

//[SECTION] Create
	module.exports.orderCheckout = (req, res) => {
		console.log(req.user.id)
		if(req.user.isAdmin){
			return res.send({message: "Action Forbidden"})
		}
		let uId = req.user.id;
		let pId = req.body.productId; 
		let qty = req.body.quantity;
		let addProduct = [{
			"productId": pId,
			"quantity": qty
		}];
		Product.findById(pId).then(result => {
			console.log(result.price)		
			let newOrders = new Order({
				userId: uId,
				orders: addProduct,
				totalAmount: qty * result.price
			});
			return newOrders.save().then((orderCreated, err) => {
				if(orderCreated) {
					console.log(newOrders);
					res.send(orderCreated);					
				} else {
					return false;
				} 
			});
		 
		})
	};

//[SECTION] Retrieve
	module.exports.orderOnCart = (req, res) => {
		let id = req.user.id;
		if(req.user.isAdmin){
			return Order.find({}).then(result => {
			res.send(result);
			});
		} else{
			return Order.find({userId: id}).then(result => {
				res.send(result);
			})
		}	
	};
