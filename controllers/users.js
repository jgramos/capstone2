//[SECTION] Dependencies and Modules
	const auth = require('../auth');
	const User = require('../models/User');
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');

//[SECTION] Environment Setup
	dotenv.config();
	const salt = parseInt(process.env.SALT);

//[SECTION] Functionalties [CREATE]
	//User Registration
	module.exports.registerUser = (req, res) => {
		console.log(req.body);
		let mail = req.body.email;
		User.findOne({email: mail}).then(userFound => {
			if (userFound != null && userFound.email == req.body.email) {
				return res.send({message:'Email Already Excist'});
			} else {				
				
				let email = req.body.email;
				let passW = req.body.password;
				let newUser = new User({
					email: email,
					password: bcrypt.hashSync(passW, salt)
					});
			 	return newUser.save().then((savedUser, err) => {
			   	 	if (err) {
			   	 		return res.send({message:'Failed to Save New User'});
			   	 	} else {
			   	 		return res.send(true); 
			   	 	}
		   	 	});
		 	};
		})
		.catch(err => res.send(err));
	};

	//User Login
	module.exports.loginUser = (req, res)  => {
		console.log(req.body);
		let mail = req.body.email;
		User.findOne({email: mail}).then(userFound => {
			if (userFound === null) {
				return res.send({message: 'User Not Found'});
			} else {
				const keyMatch = bcrypt.compareSync(req.body.password, userFound.password)
				console.log(keyMatch);
				if(keyMatch){
					return res.send({accessToken: auth.createAccessToken(userFound)});
				} else{
					return res.send({message:'Password Incorrect'});
				}
			};
		})
		.catch(err => res.send(err));
	};

//[SECTION] Functionalties [UPDATE]
	//User Admin Activation
   module.exports.adminActivation = (id) => {
      let updates = {
         isAdmin: true
      };

      return User.findByIdAndUpdate(id, updates).then((isAdminTrue, err) => {
         if (isAdminTrue) {
            return ({message:`The User with ID: ${isAdminTrue.email} has been activated`});
            console.log(req.body.isAdmin);
         } else {
            return ({message:"You don't have Admin Access"});
         };
      });
   };

   //User Admin Deactivation
   module.exports.adminDeactivation = (id) => {
      let updates = {
         isAdmin: false
      };
      return User.findByIdAndUpdate(id, updates).then((isAdminFalse, err) => {
         if (isAdminFalse) {
            return ({message:`The User with ID: ${isAdminFalse.email} has been Dactivated`});
            console.log(req.body.isAdmin);
         } else {
            return ({message:"You don't have Admin Access"});
         };
      });
   };

//[SECTION] Functionalties [RETRIEVE]
	//Get All Users
	module.exports.getAllUser = (req, res) => {
			return User.findById(req.user.id).then(result => {
				res.send(result);
			});
	};

//[SECTION] Functionalties
//[SECTION] Functionalties