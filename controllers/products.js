//[SECTION] Dependencies and Modules
	const auth = require('../auth');
	const Product = require('../models/Product');
	const User = require('../models/User');
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');

//[SECTION] Environment Setup
	dotenv.config();
	const salt = parseInt(process.env.SALT);

//[SECTION] Functionalties [CREATE]
	//Product Registration
	module.exports.createProduct = async (req, res) => {
		let pName = req.body.name;
		Product.findOne({name: pName}).then(itemExist => {
			if (itemExist != null && itemExist.name == req.body.name) {
				return res.send({message: 'Product Already Exist'});
			} else {		
				let pName = req.body.name;
				let pDesc = req.body.description;
				let price = req.body.price;
				let newProduct = new Product({
					name: pName,
					description: pDesc,
					price: price
					});
	 				return newProduct.save().then((savedProduct, err) => {
		   	 	if (err) {
		   	 		return res.send({message:'Failed to Save New Product'});
		   	 	} else {
		   	 		return res.send(savedProduct); 
		   	 	}
	   	 	});
	 		};
		});
	};	

//[SECTION] Functionalties [UPDATE]
   module.exports.isActiveTrue = (id) => {
      let updates = {
         isActive: true
      };

      return Product.findByIdAndUpdate(id, updates).then((productTrue, err) => {
  			if (productTrue) {
            return (true);
         } else {
            return ({message:'Failed to Activate Product'});
         };
      });
   };

   //Archive
   module.exports.isActiveFalse = (id) => {
      let updates = {
         isActive: false
      };

      return Product.findByIdAndUpdate(id, updates).then((productFalse, err) => {
         if (productFalse) {
            return (true);
         } else {
            return ({message:'Failed to archive Product'});
         };
      });
   };

   //Product Update 
   module.exports.updateProduct = (req, res) => {
   	let item = req.body;
   	let id = req.params.id;
   	let cName = item.name;
   	let cDesc = item.description;
   	let cCost = item.price;
   	let updateProduct = {
   		name: cName,
   		description: cDesc,
   		price: cCost
   	}
		return Product.findByIdAndUpdate(id,updateProduct).then((productUpdate, err) => {
   		if(err) {
   			res.send({message:'Failed to update Product'});
   		} else {
   			if (productUpdate.name === cName && productUpdate.description === cDesc && productUpdate.price === cCost) {
   				res.send({message:'Nothing Change'});
   			} else {
   				res.send(true);
   				console.log(updateProduct);
   			}

   		};
		});   	
   };

//[SECTION] Functionalties [RETRIEVE]
	//All Products
	module.exports.getAllProduct = (req, res) => {
		return Product.find({}).then(result => {
			res.send(result);
		});
	};

	//Single Product
	module.exports.getProduct = (req, res) => {
		let id = req.params.id;
		return Product.findById(id).then((result, err) => {
			res.send(result);				
		})	
	};
//[SECTION] Functionalties [DELETE]
 	module.exports.deleteProduct = (id) => {
    return Product.findByIdAndRemove(id).then((removeProduct, err) => {
	    if (err) {
	        return false;
	    }else{
	        return true;
	    }
    });

};
//[SECTION] Functionalties