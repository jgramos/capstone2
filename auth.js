// imports
const jwt = require("jsonwebtoken");
const secret = "ecommerce-api-batch165";

module.exports.createAccessToken = (user) => {
	console.log(user)

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {})

};

module.exports.verify = (req, res, next) => {
	const authorization = req.headers.authorization;
	console.log(authorization);
	let token = authorization;

	if (typeof token === "undefined") {
		return res.send({auth: "failed. No token"});
	} else {
		console.log(token);
		token = token.slice(7, token.length);
		console.log(token);

		jwt.verify(token, secret, function(err, decodedToken) {
			if (err) {
				return res.send({
					auth: "Failed",
					message: err.message
				});
			} else {
				console.log(decodedToken);
				req.user = decodedToken

				next();
			}
		})
	};
};

module.exports.verifyAdmin = (req, res, next) => {

	if(req.user.isAdmin){

		next();

	} else {

		return res.send({
			auth: "Failed",
			message: "You don't have Admin Access"
		})
	}
}
