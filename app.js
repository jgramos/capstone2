// * [SECTION] Packages and Dependencies
	const express = require("express");
	const mongoose = require("mongoose");
	const dotenv = require("dotenv");
	const userRoutes = require('./routes/users');
	const productRoutes = require('./routes/products');
	const orderRoutes = require('./routes/orders');
	const cors = require('cors');

// * [SECTION] Server Setup
	const app = express();
	dotenv.config();
	app.use(cors()) ;
	app.use(express.json());
	const secret = process.env.CONNECTION_STRING
	const port = process.env.PORT;

//[SECTION] Application Routes
	app.use('/users', userRoutes);
	app.use('/products', productRoutes);
	app.use('/orders', orderRoutes);

// * [SECTION] Database Connet
	mongoose.connect(secret);
	let connectStatus = mongoose.connection;
	connectStatus.once('open', () => console.log('Nakakonekta ang Databse'));

// * [SECTION] Gateway Response
	app.get('/', (req, res) => {
		res.send('Ito ay para sa mga iniwan, nasaktan, at pinaasa, Welcome sa aking Pahina! Halikana sa CAMP SAWI Resort!');
	});
	app.listen(port, () => console.log(`server is running on port ${port}`));
