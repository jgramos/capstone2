//[SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/products');
	const auth = require('../auth');
	const {verify, verifyAdmin} = auth;

//[SECTION] Routing Compoent
	const route = exp.Router();

//[SECTION] Routes - [POST]
	route.post('/create',verify, verifyAdmin, controller.createProduct);

//[SECTION] Routes - [PUT]
	//Activation
	route.put('/:id/product', verify, verifyAdmin, (req, res) => {
		let productId = req.params.id;
		controller.isActiveTrue(productId).then(isAdminTrue => {
			res.send(isAdminTrue);
		});
	});

	//Archive
	route.put('/:id/archive', verify, verifyAdmin,(req, res) => {
		let productId = req.params.id;
		controller.isActiveFalse(productId).then(isAdminFalse => {
			res.send(isAdminFalse);
		});
	});

	//Update Product
	route.put('/:id', verify, verifyAdmin,controller.updateProduct);

//[SECTION] Routes [GET]
	//route.get('/all', verify, verifyAdmin, controller.getAllProduct);
	route.get('/all', controller.getAllProduct);
	route.get('/item/:id', controller.getProduct);
	//route.get('/', controller.getActiveProduct);

//[SECTION] [DEL] Routes
route.delete('/:id', verify, verifyAdmin, (req, res) => {
  let id = req.params.id;
  controller.deleteProduct(id).then(outcome => {
    res.send(outcome);
  });
});
//[SECTION] Routes
//[SECTION] Routes
	module.exports = route;
