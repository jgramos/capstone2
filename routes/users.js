//[SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/users');
	const auth = require('../auth');
	const {verify, verifyAdmin} = auth;

//[SECTION] Routing Compoent
	const route = exp.Router();

//[SECTION] Routes - [POST]
	route.post('/register', controller.registerUser);
	route.post("/login", controller.loginUser);

//[SECTION] Routes - [PUT]
	route.put('/:id/isadmint', verify, verifyAdmin, (req, res) => {
		let userId = req.params.id;
		controller.adminActivation(userId).then(isAdminTrue => {
			res.send(isAdminTrue);
		});
	});

	route.put('/:id/isadminf', verify, verifyAdmin, (req, res) => {
		let userId = req.params.id;
		controller.adminDeactivation(userId).then(isAdminFalse => {
			res.send(isAdminFalse);
		});
	});

//[SECTION] Routes [GET]
	route.get('/all', verify, verifyAdmin, controller.getAllUser);

//[SECTION] Routes
//[SECTION] Routes
//[SECTION] Routes
	module.exports = route;
