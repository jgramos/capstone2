//[SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/orders');
	const auth = require('../auth');
	const {verify, verifyAdmin} = auth;

//[SECTION] Routing Compoent
	const route = exp.Router();

//[SECTION] Routes - [POST]
	route.post('/checkout',verify, controller.orderCheckout);

//[SECTION] Routes - [PUT]

//[SECTION] Routes - [GET]
	route.get('/cart', verify, controller.orderOnCart);

//[SECTION] Routes
	module.exports = route;
